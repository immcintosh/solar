#pragma once

#include <chrono>
#include <functional>
#include <em_core.h>
#include "util/atomic_optional.h"

#define MESOS_ATOMIC_SECTION(code) CORE_ATOMIC_SECTION(code)

namespace mesos
{
using microseconds = std::chrono::microseconds;
using namespace std::chrono_literals;
}

namespace mesos::mcu
{
}
