#include <mesos/timer.h>
#include <mesos/mcu/gpio.h>
#include <mesos/mcu/rtc.h>

using namespace mesos;

typedef mcu::gpio_out<mcu::pins::C11> load_gpio;

void mesos_reset()
{
	static timer<mcu::rtc> toggler;

	toggler.start(100ms, [load = load_gpio {}](timer<mcu::rtc>&) { load.toggle(); return true; });
}
