#pragma once

#include <atomic>
#include <functional>
#include "platform.h"

namespace mesos
{
template<class Source> class timer
{
public:
	typedef Source source_type;
	typedef typename source_type::timer_data data_type;
	typedef std::function<bool(timer&)> callback_type;
	
	timer();
	~timer();
	bool start(microseconds period, callback_type callback);
	bool stop();
	bool call() { m_callback(*this); }
	
	const data_type& data() const { return m_data; }
	microseconds period() const { return m_period; }
	void set_period(const microseconds& value) { m_period = value; }
	const callback_type& callback() const { return m_callback; }
	void set_callback(const callback_type& value) { m_callback = value; }
	
	timer(const timer&) = delete;
	timer& operator=(const timer&) = delete;
	
protected:
	microseconds m_period;
	callback_type m_callback;
	data_type m_data;
	
	bool start();
};

template<class Source> timer<Source>::timer()
{
}

template<class Source> timer<Source>::~timer()
{
	stop();
}

template<class Source> bool timer<Source>::start(microseconds period, callback_type callback)
{
	m_period = period;
	m_callback = callback;
	return start();
}
}
