# Solar Powered IoT Node

This simple compact design includes all the components necessary for implementing a fully solar powered IoT node in either 2.4G or sub-GHz bands.  Full design files are publicly available in Altium CircuitMaker.

![Device](iot_node.jpeg)