#pragma once

#include <optional>
#include "atomic_guard.h"

namespace mesos
{
template<typename T> class atomic_optional : public std::optional<T>
{
public:
	using std::optional<T>::operator bool;
	template<bool Cond, typename... Args> atomic_guard emplace_if(Args... args)
	{
		atomic_guard guard { };
		if(std::optional<T>::has_value() == Cond) {
			std::optional<T>::emplace(std::forward<Args...>(args...));
			return std::move(guard);
		}
		return { false };
	}
	atomic_guard reset_if()
	{
		atomic_guard guard { };
		if(std::optional<T>::has_value()) {
			std::optional<T>::reset();
			return std::move(guard);
		}
		return { false };
	}
};
}
