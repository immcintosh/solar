#pragma once

#include <atomic>
#include "atomic_guard.h"
#include "../platform.h"

namespace mesos
{
template<class T> class global_resource
{
public:
	template<typename... Args> global_resource(Args... args)
	{
		acquire(std::forward<Args>(args)...);
	}
	global_resource(const global_resource& other)
	{
		++m_counter;
	}
	global_resource(global_resource&& other)
	{
		++m_counter;
	}
	~global_resource()
	{
		release();
	}
	global_resource& operator=(const global_resource& other)
	{
		++m_counter;
	}
	global_resource& operator=(global_resource&& other)
	{
		++m_counter;
	}
	
private:
	typedef unsigned int counter_t;
	
	static constexpr counter_t acquire_bit = 1 << (std::numeric_limits<unsigned char>::digits * sizeof(counter_t) - 1);
	
	inline static std::atomic<counter_t> m_counter {0};
	
	template<typename... Args> void acquire(Args... args)
	{
		static_assert((acquire_bit + 1) << 1 == 2);
		if(++m_counter << 1 == 2) {
			atomic_guard atomic;
			if(m_counter == 1) {
				T::acquire(std::forward<Args>(args)...); 
			}
			m_counter |= acquire_bit;
		}
	}
	void release()
	{
		if(--m_counter == acquire_bit) {
			atomic_guard atomic;
			if(m_counter == acquire_bit) {
				T::release();
			}
			m_counter = 0;
		}
	}
};
}
