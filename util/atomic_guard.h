#pragma once

#include <em_core.h>

namespace mesos
{
class atomic_guard
{
public:
	atomic_guard()
		: atomic_guard(true)
	{
	}
	atomic_guard(bool guarded)
		: m_state(guarded ? CORE_EnterAtomic() | valid_flag : 0)
	{
	}
	atomic_guard(atomic_guard&& other)
	{
		operator =(std::forward<atomic_guard&&>(other));
	}
	atomic_guard& operator =(atomic_guard&& other)
	{
		m_state = other.m_state;
		other.m_state = 0;
		return *this;
	}
	~atomic_guard()
	{
		if(m_state & valid_flag) {
			CORE_ExitAtomic(m_state ^ valid_flag);
		}
	}
	operator bool() const
	{
		return m_state & valid_flag;
	}
	
private:
	/*! @note This bit is reserved on Cortex M, so we use it to indicate a valid state. */
	static constexpr CORE_irqState_t valid_flag = 0x80000000;
	
	atomic_guard(const atomic_guard&) = delete;
	atomic_guard operator=(const atomic_guard&) = delete;
	
	CORE_irqState_t m_state;
};
}
