#pragma once

namespace mesos
{
template<class Source> class calendar
{
protected:
	typedef Source source_type;
	typedef typename source_type::calendar_data data_type;
};
}
