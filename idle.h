#pragma once

#include <functional>
#include "platform.h"

namespace mesos
{
void idle(microseconds wakeup);
[[noreturn]] void idle_loop();
[[noreturn]] void idle_loop(const std::function<microseconds()>& process);
}
