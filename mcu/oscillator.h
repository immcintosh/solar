#pragma once

#include <em_cmu.h>
#include "../util/global_resource.h"

namespace mesos::mcu
{
template<CMU_Osc_TypeDef ID, CMU_Select_TypeDef Select> struct oscillator_id
{
	static constexpr CMU_Osc_TypeDef value = ID;
	static constexpr CMU_Select_TypeDef select = Select;
};

template<typename ID> class oscillator
{
public:
	typedef ID id;
	
	static void acquire()
	{
		CMU_OscillatorEnable(id::value, true, true);
	}
	static void release()
	{
		CMU_OscillatorEnable(id::value, false, false);
	}
	
private:
	global_resource<oscillator<ID>> m_resource;
};
}

namespace mesos::mcu::oscillators
{
typedef oscillator<oscillator_id<cmuOsc_LFXO, cmuSelect_LFXO>> lfxo;
typedef oscillator<oscillator_id<cmuOsc_LFRCO, cmuSelect_LFRCO>> lfrco;
typedef oscillator<oscillator_id<cmuOsc_HFXO, cmuSelect_HFXO>> hfxo;
typedef oscillator<oscillator_id<cmuOsc_HFRCO, cmuSelect_HFRCO>> hfrco;
typedef oscillator<oscillator_id<cmuOsc_AUXHFRCO, cmuSelect_AUXHFRCO>> auxhfrco;
typedef oscillator<oscillator_id<cmuOsc_ULFRCO, cmuSelect_ULFRCO>> ulfrco;
}

namespace mesos::mcu
{
template<> void oscillators::lfxo::acquire();
template<> void oscillators::hfxo::acquire();
}
