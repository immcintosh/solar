#pragma once

#include <em_cmu.h>
#include "interrupt.h"
#include "../timer.h"
#include "../util/global_resource.h"

namespace mesos::mcu
{
class systick
{
public:
	struct timer_data;
	
	static void acquire()
	{
		SysTick->LOAD = SysTick_LOAD_RELOAD_Msk;
		SysTick->VAL = 0UL;
		SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
	}
	static void release()
	{
		SysTick->CTRL = 0;
	}
	
private:
	global_resource<systick> m_resource;
};

struct systick::timer_data
{
	static std::atomic<timer<mcu::systick>*> active;
	systick source;
	interrupts::systick interrupt;
};
}

namespace mesos
{
template<> bool timer<mcu::systick>::start();
template<> bool timer<mcu::systick>::stop();
}
