#pragma once

#include <em_rtcc.h>
#include "clock.h"
#include "interrupt.h"
#include "../timer.h"
#include "../util/global_resource.h"

namespace mesos::mcu
{
class rtc
{
public:
	typedef timer<rtc> timer_t;
	struct timer_data;
	
	static void acquire()
	{
		RTCC_Init_TypeDef settings = {
			.enable = true,
			.debugRun = false,
			.precntWrapOnCCV0 = false,
			.cntWrapOnCCV1 = false,
			.presc = rtccCntPresc_1,
			.prescMode = rtccCntTickPresc,
			.enaOSCFailDetect = false,
			.cntMode = rtccCntModeNormal,
			.disLeapYearCorr = false,
		};
		RTCC_Init(&settings);
	}
	static void release()
	{
		RTCC_Reset();
	}
	
private:
	clocks::hfle m_clock_hfle;
	clocks::lfe m_clock_lfe;
	clocks::rtcc m_clock_rtcc;
	global_resource<rtc> m_resource;
};

struct rtc::timer_data
{
	static std::atomic<timer<mcu::rtc>*> active;
	rtc source;
	interrupts::rtc interrupt;
	uint32_t period;
};
}

namespace mesos
{
template<> bool timer<mcu::rtc>::start();
template<> bool timer<mcu::rtc>::stop();
}
