#include "core.h"

#include <em_chip.h>
#include "../idle.h"

__attribute__((weak)) void mesos_reset()
{
}

__attribute__((weak)) mesos::microseconds mesos_process()
{
	return mesos::microseconds::max();
}

extern "C" int main()
{
	CHIP_Init();
	
	mesos_reset();
	
	mesos::idle_loop(mesos_process);
}
