#pragma once

#include <atomic>
#include <em_cmu.h>
#include "oscillator.h"
#include "../platform.h"
#include "../util/global_resource.h"

namespace mesos::mcu
{
template<CMU_Clock_TypeDef ID> struct clock_id
{
	static constexpr CMU_Clock_TypeDef value = ID;
};

template<typename ID> struct clock_default
{
	typedef oscillators::hfrco oscillator;
};

template<typename ID> struct clock_config
{
	typedef typename clock_default<ID>::oscillator oscillator;
};

template<> struct clock_config<clock_id<cmuClock_HFLE>>
{
	typedef oscillators::hfrco oscillator;
};

template<> struct clock_config<clock_id<cmuClock_LFE>>
{
	typedef oscillators::lfxo oscillator;
};

template<typename ID, typename Oscillator = typename clock_config<ID>::oscillator> class clock
{
public:
	typedef ID id;
	typedef Oscillator oscillator_type;
	
	static void acquire()
	{
		CMU_ClockSelectSet(id::value, oscillator_type::id::select);
		CMU_ClockEnable(id::value, true);
	}
	static void release()
	{
		CMU_ClockEnable(id::value, false);
	}
	static uint32_t frequency()
	{
		return CMU_ClockFreqGet(id::value);
	}
	
private:
	oscillator_type m_oscillator;
	global_resource<clock<id>> m_resource;
};
}

namespace mesos::mcu::clocks
{
typedef clock<clock_id<cmuClock_CORE>> core;
typedef clock<clock_id<cmuClock_HFLE>> hfle;
typedef clock<clock_id<cmuClock_GPIO>> gpio;
typedef clock<clock_id<cmuClock_RTCC>> rtcc;
typedef clock<clock_id<cmuClock_LFE>> lfe;
typedef clock<clock_id<cmuClock_HF>> hf;
typedef clock<clock_id<cmuClock_DBG>> dbg;
typedef clock<clock_id<cmuClock_AUX>> aux;
typedef clock<clock_id<cmuClock_BUS>> bus;
typedef clock<clock_id<cmuClock_CRYPTO>> crypto;
typedef clock<clock_id<cmuClock_LDMA>> ldma;
typedef clock<clock_id<cmuClock_GPCRC>> gpcrc;
typedef clock<clock_id<cmuClock_GPIO>> gpio;
typedef clock<clock_id<cmuClock_PRS>> prs;
typedef clock<clock_id<cmuClock_HFPER>> hfper;
typedef clock<clock_id<cmuClock_USART0>> usart0;
typedef clock<clock_id<cmuClock_USART1>> usart1;
typedef clock<clock_id<cmuClock_TIMER1>> timer1;
typedef clock<clock_id<cmuClock_CRYOTIMER>> cryotimer;
typedef clock<clock_id<cmuClock_ACMP0>> acmp0;
typedef clock<clock_id<cmuClock_ACMP1>> acmp1;
typedef clock<clock_id<cmuClock_IDAC0>> idac0;
typedef clock<clock_id<cmuClock_ADC0>> adc0;
typedef clock<clock_id<cmuClock_I2C0>> i2c0;
typedef clock<clock_id<cmuClock_CORE>> core;
typedef clock<clock_id<cmuClock_LFA>> lfa;
typedef clock<clock_id<cmuClock_LETIMER0>> letimer0;
typedef clock<clock_id<cmuClock_PCNT0>> pcmt;
typedef clock<clock_id<cmuClock_LFB>> lfb;
typedef clock<clock_id<cmuClock_LEUART0>> leuart0;
typedef clock<clock_id<cmuClock_TIMER0>> timer0;
}
