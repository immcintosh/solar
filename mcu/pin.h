#pragma once

#include <em_gpio.h>
#include "../platform.h"

namespace mesos::mcu
{
typedef GPIO_Port_TypeDef pin_port_t;

typedef uint8_t pin_number_t;

class pin_owner {};

template<pin_port_t Port, pin_number_t Number> struct pin
{
	static constexpr pin_port_t port = Port;
	static constexpr pin_number_t number = Number;
};
}

namespace mesos::mcu::pins
{
typedef pin<gpioPortF, 0> A0;
typedef pin<gpioPortF, 1> A1;
typedef pin<gpioPortF, 2> A2;
typedef pin<gpioPortF, 3> A3;
typedef pin<gpioPortF, 4> A4;
typedef pin<gpioPortF, 5> A5;

typedef pin<gpioPortB, 11> B11;
typedef pin<gpioPortB, 12> B12;
typedef pin<gpioPortB, 13> B13;
typedef pin<gpioPortB, 14> B14;
typedef pin<gpioPortB, 15> B15;

typedef pin<gpioPortC, 6> C6;
typedef pin<gpioPortC, 7> C7;
typedef pin<gpioPortC, 8> C8;
typedef pin<gpioPortC, 9> C9;
typedef pin<gpioPortC, 10> C10;
typedef pin<gpioPortC, 11> C11;

typedef pin<gpioPortD, 13> D13;
typedef pin<gpioPortD, 14> D14;
typedef pin<gpioPortD, 15> D15;

typedef pin<gpioPortF, 0> F0;
typedef pin<gpioPortF, 1> F1;
typedef pin<gpioPortF, 2> F2;
typedef pin<gpioPortF, 3> F3;
typedef pin<gpioPortF, 4> F4;
typedef pin<gpioPortF, 5> F5;
typedef pin<gpioPortF, 6> F6;
typedef pin<gpioPortF, 7> F7;
}
