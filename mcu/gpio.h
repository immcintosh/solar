#pragma once

#include <em_gpio.h>
#include "clock.h"
#include "pin.h"
#include "../util/global_resource.h"

namespace mesos::mcu
{
template<typename Pin> struct gpio_config
{
	static constexpr GPIO_Mode_TypeDef reset_mode = gpioModeDisabled;
	static constexpr GPIO_Mode_TypeDef out_mode = gpioModePushPull;
	static constexpr GPIO_Mode_TypeDef in_mode = gpioModeInput;
	static constexpr bool reset_value = false;
	static constexpr bool out_value = false;
	static constexpr bool in_value = false;
};

template<typename Pin> class gpio
{
public:
	static void release()
	{
		set_mode(gpio_config<Pin>::reset_mode);
		BUS_RegBitWrite(&GPIO->P[Pin::port].DOUT, Pin::number, gpio_config<Pin>::reset_value);
	}
	
protected:
	gpio() = default;
	
	static void set(bool value)
	{
		BUS_RegBitWrite(&GPIO->P[Pin::port].DOUT, Pin::number, value);
	}
	static bool get()
	{
		return BUS_RegBitRead(&GPIO->P[Pin::port].DOUT, Pin::number);
	}

	static void set_mode(GPIO_Mode_TypeDef mode)
	{
		if constexpr(Pin::number < 8) {
			GPIO->P[Pin::port].MODEL = (GPIO->P[Pin::port].MODEL & ~(0xFu << (Pin::number * 4)))
			                      | (mode << (Pin::number * 4));
		} else {
			GPIO->P[Pin::port].MODEH = (GPIO->P[Pin::port].MODEH & ~(0xFu << ((Pin::number - 8) * 4)))
			                      | (mode << ((Pin::number - 8) * 4));
		}
	}
	
private:
	clocks::gpio m_clock;
};

template<typename Pin> class gpio_out : public gpio<Pin>
{
public:
	gpio_out() = default;
	gpio_out(bool value)
		: m_resource(value)
	{
	}
	using gpio<Pin>::get;
	using gpio<Pin>::set;
	static void toggle()
	{
		GPIO->P[Pin::port].DOUTTGL = 1 << Pin::number;
	}
	
	static void acquire(bool value = gpio_config<Pin>::reset_value)
	{
		set(value);
		gpio<Pin>::set_mode(gpio_config<Pin>::out_mode);
	}
	using gpio<Pin>::release;
	
private:
	global_resource<gpio_out<Pin>> m_resource;
};

template<typename Pin> class gpio_in : public gpio<Pin>
{
public:
	bool get() const
	{
		return BUS_RegBitRead(&GPIO->P[Pin::port].DIN, Pin::number);
	}
	static bool get_pull() { return gpio<Pin>::get; }
	static void set_pull(bool value) { gpio<Pin>::set(value); }
	
	static void acquire()
	{
		set_pull(gpio_config<Pin>::in_value);
		gpio<Pin>::set_mode(gpio_config<Pin>::in_mode);
	}
	using gpio<Pin>::release;
	
private:
	global_resource<gpio_in<Pin>> m_resource;
};
}
