#include "rtc.h"

namespace mesos::mcu
{
std::atomic<timer<mcu::rtc>*> rtc::timer_data::active {nullptr};
}

namespace mesos
{
template<> bool timer<mcu::rtc>::start()
{
	atomic_guard g;
	if(data().active.load(std::memory_order_acquire) != nullptr) {
		return false;
	}
	m_data.period = uint32_t(mcu::clocks::lfe::frequency()) * uint32_t(period().count()) / 1000000u;
	static RTCC_CCChConf_TypeDef init =
	{
		rtccCapComChModeCompare,
		rtccCompMatchOutActionPulse,
		rtccPRSCh0,
		rtccInEdgeNone,
		rtccCompBaseCnt,
		0,
		rtccDayCompareModeMonth
	};
	RTCC_ChannelInit(0, &init);
	RTCC_IntClear(RTCC_IF_CC0);
	RTCC_CounterSet(0);
	RTCC_ChannelCCVSet(0, RTCC_CounterGet() + m_data.period);
	RTCC_IntEnable(RTCC_IF_CC0);
	data().active.store(this, std::memory_order_release);
	m_data.interrupt.enable();
	return true;
}

template<> bool timer<mcu::rtc>::stop()
{
	atomic_guard g;
	if(data().active.load(std::memory_order_acquire) != this) {
		return false;
	}
	RTCC_IntDisable(RTCC_IF_CC0);
	data().active.store(nullptr, std::memory_order_release);
	return true;
}
}

extern "C" void RTCC_IRQHandler(void)
{
	using namespace mesos;
	timer<mcu::rtc>* timer = mcu::rtc::timer_data::active.load(std::memory_order_acquire);
	RTCC_IntClear(RTCC_IF_CC0);
	if(timer && !timer->call()) {
		timer->stop();
	} else {
		RTCC_ChannelCCVSet(0, RTCC_ChannelCCVGet(0) + timer->data().period);
	}
}
