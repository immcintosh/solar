#include "oscillator.h"

namespace mesos::mcu
{
template<> void oscillators::lfxo::acquire()
{
	CMU_LFXOInit_TypeDef init {
		.ctune = 0,
		.gain = 0,
		.timeout = 0,
		.mode = cmuOscMode_Crystal,
	};
	CMU_LFXOInit(&init);
	CMU_OscillatorEnable(id::value, true, true);
}

template<> void oscillators::hfxo::acquire()
{
}
}
