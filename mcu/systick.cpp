#include "systick.h"

#include "clock.h"

namespace mesos::mcu
{
std::atomic<timer<mcu::systick>*> systick::timer_data::active{nullptr};
}

namespace mesos
{
template<> bool timer<mcu::systick>::start()
{
	atomic_guard g;
	if(data().active.load(std::memory_order_acquire) != nullptr) {
		return false;
	}
	data().active.store(this, std::memory_order_release);
	SysTick->CTRL = 0;
	SysTick->LOAD = uint32_t(mcu::clocks::core::frequency() / 1000000) * uint32_t(period().count()) - 1;
	SysTick->VAL = 0;
	SysTick->CTRL =
		SysTick_CTRL_CLKSOURCE_Msk |
		SysTick_CTRL_TICKINT_Msk |
		SysTick_CTRL_ENABLE_Msk;
	return true;
}

template<> bool timer<mcu::systick>::stop()
{
	atomic_guard g;
	if(data().active.load(std::memory_order_acquire) != this) {
		return false;
	}
	m_data.interrupt.disable();
	SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
	data().active.store(nullptr, std::memory_order_release);
	return true;
}
}

extern "C" void SysTick_Handler()
{
	using namespace mesos;
	timer<mcu::systick>* timer = mcu::systick::timer_data::active.load(std::memory_order_acquire);
	if(timer && !timer->call()) {
		timer->stop();
	}
}
