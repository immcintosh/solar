#pragma once

#include <functional>
#include <em_core.h>
#include "../util/global_resource.h"

namespace mesos::mcu
{
template<IRQn_Type ID> struct interrupt_id
{
	static constexpr IRQn_Type value = ID;
};

template<typename ID> struct interrupt_config
{
	static constexpr uint8_t priority = 0;
};

template<typename ID> class interrupt
{
public:
	static void acquire()
	{
		NVIC_SetPriority(ID::value, interrupt_config<ID>::priority);
		clear();
	}
	static void release()
	{
		disable();
	}
	static void enable()
	{
		NVIC_ClearPendingIRQ(ID::value);
		NVIC_EnableIRQ(ID::value);
	}
	static void disable()
	{
		NVIC_DisableIRQ(ID::value);
	}
	static void clear()
	{
		NVIC_ClearPendingIRQ(ID::value);
	}
	
private:
	global_resource<interrupt<ID>> m_resource;
};
}

namespace mesos::mcu::interrupts
{
typedef mcu::interrupt<mcu::interrupt_id<SysTick_IRQn>> systick;
typedef mcu::interrupt<mcu::interrupt_id<RTCC_IRQn>> rtc;
}
