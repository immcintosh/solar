#include "idle.h"

#include <em_emu.h>
#include "timer.h"
#include "mcu/systick.h"

namespace mesos
{
void idle(microseconds wakeup)
{
	if(wakeup != microseconds::max()) {
		timer<mcu::systick>().start(wakeup, [](timer<mcu::systick>&) { return false; });
	}
	EMU_EnterEM2(true);
}

void idle_loop()
{
	idle_loop([]() { return microseconds::max(); });
}

void idle_loop(const std::function<microseconds()>& process)
{
	for(;;) {
		microseconds wait = process();
		if(wait != 0us) {
			idle(wait);
		}
	}
}
}
